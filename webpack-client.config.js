const path = require("path");
const webpack = require("webpack");
const { merge } = require('webpack-merge');
const base = require('./webpack-base.config');
const TerserJSPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, argv) => merge(base, {
	entry: "./src/index.jsx",
	optimization: {
		minimizer: [new TerserJSPlugin(), new CssMinimizerPlugin()],
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.s[ac]ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					{
						loader: 'css-loader',
						options: {
							esModule: false,
						}
					},
					"postcss-loader",
					"sass-loader"
				]
			},
		]
	},
	output: {
		path: path.resolve(__dirname, "dist/"),
		filename: argv.mode === 'development' ? "bundle.js" : "bundle-[chunkhash].js",
		publicPath: '/',
	},
	plugins: argv.mode === 'development'
		? [
			new MiniCssExtractPlugin(),
			new HtmlWebpackPlugin({
				template: 'src/index.html',
				filename: 'index.html',
				favicon: 'src/assets/favicon.png',
			}),
		]
		: [
			new MiniCssExtractPlugin({
				filename: '[name]-[chunkhash].css',
			}),
			new HtmlWebpackPlugin({
				template: 'src/index.html',
				filename: 'nossr-index.html',
				favicon: 'src/assets/favicon.png',
			}),
		],
	devServer: {
		host: '0.0.0.0',
		historyApiFallback: { index: '/' },
	},
});
