const path = require("path");
const webpack = require("webpack");
const { merge } = require('webpack-merge');
const base = require('./webpack-base.config');

module.exports = merge(base, {
	entry: "./src/static-render.jsx",
	module: {
		rules: [
			{
				test: /\.s[ac]ss$/,
				loader: 'null-loader'
			},
		]
	},
	output: {
		path: path.resolve(__dirname, "build/"),
		filename: "static-render-bundle.js",
		publicPath: '/',
	},
	target: "node",
});
