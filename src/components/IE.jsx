import React, { Component} from "react";
import About from './About';
import Concerts from './Concerts';
import Promo from './Promo';
import Contact from './Contact';

export default class IE extends Component {
	render() {
		return(
			<div className="IE">
				<div className="ieWarning">
					Your browser is pretty old and unsupported. Please, use another one - for example Firefox, Chrome, Opera
					or, if you can't help it, Edge. Thank you for understanding (also, we didn't bother to translate
				  the website for Internet Explorer users).
				</div>
				<div className="ieWarning">
					Váš prohlížeč je hrozně starý a již není podporován. Použijte prosím jiný - například Firefox,
					Chrome, Opera nebo v nejhorším případě Edge. Děkujeme za pochopení.
				</div>
				<div className="ieWarning">
					Některé funkce webu nejsou v tomto prohlížeči k dispozici. Navíc vypadá v Internet Exploreru ošklivě!
				</div>

				<section>
					<h1>O kapele</h1>
					<About/>
				</section>

				<section>
					<h2>Koncerty</h2>
					<Concerts/>
				</section>

				<section>
					<h2>Pro pořadatele</h2>
					<Promo/>
				</section>

				<section>
					<h2>Kontakt</h2>
					<Contact/>
				</section>

				<span>Používáme cookies nutné pro provoz webu a&nbsp;analytiku.</span>
			</div>
		);
	}
}
