import React, { Component } from 'react';
import Icon from './Icon';
import { albumSongs, splitTitle, parseTrackNumber } from '../util/songs';
import { playerService } from '../service/playerService';
import './Salt.scss';

const saltSongs = albumSongs('Salt');

import saltCover from '../assets/salt-cover.jpg?size=600';
import saltBackcover from '../assets/salt-backcover.svg';

export default class Salt extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showDownloadLinks: false,
			paused: true,
		};
		this.showDownloadLinks_ = this.showDownloadLinks_.bind(this);
		this.hideDownloadLinks_ = this.hideDownloadLinks_.bind(this);
		this.togglePlay_ = this.togglePlay_.bind(this);
		this.onPlayState_ = this.onPlayState_.bind(this);
	}

	componentDidMount() {
		playerService.playStateChanges.addListener(this.onPlayState_);
	}

	componentWillUnmount() {
		playerService.playStateChanges.removeListener(this.onPlayState_);
	}

	render() {
		const songItems = saltSongs.map((song, songIndex) => {
			const titles = splitTitle(song);
			const trackNumber = parseTrackNumber(song);
			const className = [
				'unstyled-button',
				'salt-track',
				(trackNumber.trackNumber > trackNumber.total) ? 'salt-bonusTrack' : 'salt-regularTrack',
				(trackNumber.trackNumber === 1) ? 'salt-firstRegularTrack' : '',
				(trackNumber.trackNumber === trackNumber.total) ? 'salt-lastRegularTrack' : '',
			].join(' ');

			return <button key={song.name} className={className} onClick={() => this.selectSong_(songIndex)}>
				<span className="salt-songTitle">{titles.title}</span>
				{ titles.subtitle && <span className="salt-songSubtitle">{titles.subtitle}</span>}
			</button>;
		});

		const downloadLinks = saltSongs.map((song, index) => {
			const onClick = this.downloadSong_.bind(this, index);
			const shortName = song.name.replace('-cybergrounds', '');
			return <a key={song.name} download href={song.file} onClick={onClick}>{shortName}</a>;
		});

		return <div className="salt-wrapper">
			<div className={'salt' + (this.state.paused ? '' : ' salt-playing')}>
				<img src={saltCover.src}/>
				<div className="salt-content">
					<div className="salt-backCover" dangerouslySetInnerHTML={{__html: saltBackcover }}/>
					<div className="salt-trackList">
						{songItems}
					</div>
					<button className="salt-play unstyled-button" onClick={this.togglePlay_}>
						<Icon type="play" />
						<Icon type="pause" />
					</button>
					<button
						className="salt-download unstyled-button"
						onFocus={this.showDownloadLinks_}
						onBlur={this.hideDownloadLinks_}
					>
						<Icon type="download" />
						{this.state.showDownloadLinks && 
							<div className="salt-downloadLinks">{downloadLinks}</div>
						}
					</button>
				</div>
			</div>
		</div>;
	}

	showDownloadLinks_() {
		this.setState({ showDownloadLinks: true });
	}

	hideDownloadLinks_() {
		setTimeout(() => {
			this.setState({ showDownloadLinks: false });
		}, 200);
	}

	togglePlay_() {
		playerService.togglePlay();
	}

	onPlayState_(state) {
		this.setState({ paused: state.paused });
	}

	selectSong_(index) {
		playerService.selectSong(index, true);
		playerService.play();
	}

	downloadSong_(index) {
		console.log('dl', index);
		ga('send', 'event', 'Music', 'Download', 'Salt', index);
	}
}
