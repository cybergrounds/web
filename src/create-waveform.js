const path = require('path');
const fs = require('fs');
const nodeWav = require('node-wav');
const { createCanvas } = require('canvas');

const CHUNK_SIZE = 16384;
const RESOLUTION = 48;
const CHANNELS = 2;

const wavName = process.argv[2];
const pngName = process.argv[3];
if (!wavName || !pngName) {
	throw Error('Usage: node src/create-waveform.js WAV_FILE PNG_FILE');
}

const wavContents = fs.readFileSync(wavName);
const audio = nodeWav.decode(wavContents);

const length = audio.channelData[0].length;
const chunks = Math.ceil(length / CHUNK_SIZE);

const canvas = createCanvas(chunks, CHANNELS * RESOLUTION);
const canvasCtx = canvas.getContext('2d');

for (let channel = 0; channel < 2; channel++) {
	const channelData = audio.channelData[channel];
	let peaks = '';
	let sample = 0;
	for (let chunk = 0; chunk < chunks; chunk++) {
		const chunkLimit = Math.min(length, sample + CHUNK_SIZE);
		const chunkSamples = chunkLimit - sample;
		let sumOfSquares = 0;
		let peak = 0;
		while(sample < chunkLimit) {
			const value = channelData[sample];
			peak = Math.max(Math.abs(value), peak);
			sumOfSquares += value * value;
			sample++;
		}
		const rms = Math.sqrt(sumOfSquares / chunkSamples);
		drawLine(canvasCtx, channel, chunk, '#808080', peak);
		drawLine(canvasCtx, channel, chunk, '#ffffff', rms);
	}
}

const canvasBuffer = canvas.toBuffer('image/png');
fs.writeFileSync(pngName, canvasBuffer);

function drawLine(canvasCtx, channel, chunk, color, value) {
	const log = value;
	const height = Math.floor(RESOLUTION * log) + 1;
	const startY = channel ? RESOLUTION : RESOLUTION - height;
	canvasCtx.fillStyle = color;
	canvasCtx.fillRect(chunk, startY, 1, height);
}
