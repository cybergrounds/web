import 'core-js';
import 'regenerator-runtime';

import path from 'path';
import React from 'react';
import { renderToString } from "react-dom/server";
import { StaticRouter } from 'react-router-dom';
import App from "../src/components/App.jsx";
import IE from '../src/components/IE.jsx';
import { gallery, photosForCollection } from './util/photos';
const fs = require('fs');


const DIST_DIR = './dist';
const CLIENT_INDEX = path.join(DIST_DIR, 'nossr-index.html');

const template = fs.readFileSync(CLIENT_INDEX, { encoding: 'utf8' })
	.replace('<!--IS-SSR-->', '<script>var ssrRendered=true;</script>');

const templateCs = translateTemplate(template, 'cs');
const templateEn = translateTemplate(template, 'en');

const rootCanonical = createCanonical('');

renderLanguage(templateCs, '/', DIST_DIR, '', '');
renderLanguage(templateEn, '/en', path.join(DIST_DIR, 'en'), '/en', 'en');

function renderLanguage(template, rootPath, directory, subPathPrefix, canonicalSuffix) {
	const canonical = createCanonical(canonicalSuffix);
	fs.mkdirSync(directory, { recursive: true });
	renderLocation(template, rootPath, path.join(directory, 'index.html'));

	gallery.collections.forEach(collection => {
		const dir = path.join(directory, 'photo', collection.dir);
		fs.mkdirSync(dir, { recursive: true });
		photosForCollection(collection).forEach((photo, index) => {
			renderLocation(template, subPathPrefix + `/photo/${collection.dir}/${index}`, path.join(dir, index + '.html'), canonical);
		});
	});
}

renderIE(templateCs);

fs.unlinkSync(CLIENT_INDEX);

function renderLocation(template, location, file, canonical) {
	console.log('Render: "' + location + '" to ' + file);
	const renderedApp = renderToString(
		<StaticRouter location={location}>
			<App/>
		</StaticRouter>
	);
	const index = template
		.replace(/<\/head>/, `${canonical ? canonical: ''}\n$&`)
		.replace('<!--APP-->', renderedApp);
	fs.writeFileSync(file, index, { encoding: 'utf8' });
}

function renderIE(template) {
	const ieStyle = fs.readFileSync('./src/styles/ie.css');
	const ie = template
		.replace(/<\/head>/, `${rootCanonical}\n$&`)
		.replace(/<script.*<\/script>/g, '')
		.replace(/<link href=".*?" rel="stylesheet">/g, `<style>${ieStyle}</style>`)
		.replace('<!--APP-->', renderToString(<IE/>));
	fs.writeFileSync(path.join(DIST_DIR, 'ie.html'), ie, { encoding: 'utf8' });
}

function createCanonical(suffix) {
	return `<link rel="canonical" href="https://cybergrounds.cz/${suffix}" />`;
}

function translateTemplate(template, language) {
	return template
		.replace('<html lang="cs">', `<html lang="${language}">`)
		.replace(/^\s*<meta name="description" lang="(\w+)".*>/mg, (match, metaLang) => {
			if (metaLang !== language) {
				return '';
			} else {
				return match;
			}
		});
}
