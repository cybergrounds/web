const path = require('path');
const fs = require('fs');
const loaderUtils = require('loader-utils');
const NodeID3 = require('node-id3');
const nodeWav = require('node-wav');
const { createCanvas } = require('canvas');

const CHUNK_SIZE = 16384;
const RESOLUTION = 48;
const CHANNELS = 2;

module.exports = function(content) {
	const loaderContext = this;
	const loaderCallback = loaderContext.async();
	const name = path.basename(loaderContext.resource);
	const imgName = loaderContext.resource.replace(/mp3$/, 'png');

	if (!fs.existsSync(imgName)) {
		throw Error('File ' + imgName + ' not found, create it using "node src/create-waveform.js ..."');
	}

	loaderContext.emitFile(path.basename(loaderContext.resource), content);

	const result = {
		name,
		file: '/' + name,
	};

	result.tags = NodeID3.read(content);

	loaderContext.addDependency(imgName);
	const targetImgFile = path.basename(imgName);
	result.imgFile = '/' + targetImgFile;
	loaderContext.emitFile(targetImgFile, fs.readFileSync(imgName));

	return loaderCallback(null, `module.exports = ${JSON.stringify(result)};`);
}

module.exports.raw = true;
