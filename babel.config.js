module.exports = {
    "presets": [
       	[
       		"@babel/env",
       		{
       			"useBuiltIns": "entry",
       			"corejs": "3.6",
       		},
       	],
        "@babel/preset-react",
    ],
};
