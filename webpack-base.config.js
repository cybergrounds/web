const path = require("path");
const webpack = require("webpack");
const sharpAdapter = require('responsive-loader/sharp');

module.exports = {
	mode: "development",
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: "babel-loader",
			},
			{
				test: /\.png$/,
				type: 'asset/resource',
			},
			{
				test: /\.svg$/,
				loader: 'svg-inline-loader'
			},
			{
				test: /\.jpe?g$/i,
				resourceQuery: /thumbnail/,
				loader: 'responsive-loader',
				options: {
					adapter: sharpAdapter,
					size: 150,
					name: '[hash]-[width].[ext]',
				}
			},
			{
				test: /\.jpe?g$/i,
				loader: 'responsive-loader',
				options: {
					adapter: sharpAdapter,
				}
			},
			{
				test: /\.mp3$/i,
				loader: './src/loaders/song-loader.js',
			},
			{
				test: /\.woff$/,
				type: 'asset/resource',
			},
			{
				test: /\.ttf$/,
				type: 'asset/resource',
			},
			{
				test: /\.xml$/,
				type: 'asset/resource',
				generator: {
					filename: '[name].[ext]',
				}
			},
		]
	},
	resolve: { extensions: ["*", ".js", ".jsx"] },
};
